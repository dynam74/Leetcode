def uncompress(s):
  numbers = "0123456789"
  i = 0
  j = 0
  answer = []
  while j < len(s):
    if s[j] in numbers:
      j += 1
    else:
      number = int(s[i:j])
      answer.append(number*s[j])
      j += 1
      i = j
  return ''.join(answer)

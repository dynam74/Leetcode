def compress(s):
  s += '1'
  i = 0
  j = 0
  answer = []
  while j < len(s):
    if s[j] == s[i]:
      j += 1
    else:
      if (j-i) == 1:
        answer.append(s[i])
      else:
        num_letter = str(j-i) + s[i]
        answer.append(num_letter)
      i = j
  return ''.join(answer)

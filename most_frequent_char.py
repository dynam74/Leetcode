def most_frequent_char(s):
map = {}
for i in s:
    if i not in map:
    map[i] = 0
    else:
    map[i] += 1
max = 0
answer = ''
for k in map:
    if map[k] > max:
    max = map[k]
    answer = k

return answer
